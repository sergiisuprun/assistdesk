CREATE TABLE IF NOT EXISTS projects (
  id     integer      NOT NULL,
  name   varchar(150) NOT NULL,
  active boolean DEFAULT false,
  CONSTRAINT projects_pkey
  PRIMARY KEY (id)
);
