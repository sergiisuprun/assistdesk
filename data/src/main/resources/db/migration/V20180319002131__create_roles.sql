CREATE TABLE IF NOT EXISTS roles (
  id          serial      NOT NULL,
  role        varchar(50) NOT NULL,
  name        varchar(50),
  description varchar(100),
  CONSTRAINT roles_pkey
  PRIMARY KEY (id),
  CONSTRAINT roles_role_key
  UNIQUE (role)
);

