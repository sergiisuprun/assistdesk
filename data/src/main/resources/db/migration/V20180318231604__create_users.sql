CREATE TABLE IF NOT EXISTS users (
  id       serial      NOT NULL,
  username varchar(30) NOT NULL,
  password varchar(60) NOT NULL,
  enabled  boolean     NOT NULL DEFAULT true,
  CONSTRAINT users_pkey
  PRIMARY KEY (id),
  CONSTRAINT users_name_key
  UNIQUE (username)
);
