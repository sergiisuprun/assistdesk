CREATE TABLE IF NOT EXISTS users_roles (
  user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT users_roles_index_full
  PRIMARY KEY (user_id, role_id),
  CONSTRAINT foreign_key_user
  FOREIGN KEY (user_id)
  REFERENCES users (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT foreign_key_role
  FOREIGN KEY (role_id)
  REFERENCES roles (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
