package com.geekhub.ad.redmine;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.geekhub.ad.contracts.repositories.IssueRepository;
import com.geekhub.ad.contracts.repositories.SpentTimeRepository;
import com.geekhub.ad.entities.Issue;
import com.geekhub.ad.exceptions.NotFoundException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RedmineRepository implements IssueRepository, SpentTimeRepository {

    private final String server;
    private final String key;

    public RedmineRepository(String server, String key) {
        this.server = server;
        this.key = key;
    }

    @Override
    public List<Issue> findAllIssues() {
        return getIssues(server + "/issues.json");
    }

    @Override
    public List<Issue> findProjectIssues(int projectId) {
        return getIssues(server + "/issues.json?project_id=" + projectId);
    }

    private List<Issue> getIssues(String request) {
        // TODO:  "total_count":378,"offset":0,"limit":25
        try {
            URL url = new URL(request);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            setHeaderProperties(con);

            if (con.getResponseCode() != 200) {
                throw new NotFoundException("server: " + server + " response code: " + con.getResponseCode());
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            StringBuilder content = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            return getIssuesFromJson(content.toString());
        } catch (Exception e) {
            throw new NotFoundException("can't found any available issues", e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<Issue> getIssuesFromJson(String json) throws IOException {
        List<Issue> issues = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        Map map = mapper.readValue(json, Map.class);
        List<Map> issuesObject = (List<Map>) map.get("issues");

        for (Map issueObject : issuesObject) {
            Map projectObject = (Map) issueObject.get("project");
            Issue issue = new Issue();
            issue.setId((Integer) issueObject.get("id"));
            issue.setSubject((String) issueObject.get("subject"));
            issue.setDescription((String) issueObject.get("description"));
            issue.setProjectId((Integer) projectObject.get("id"));
            issue.setProjectName((String) projectObject.get("name"));
            issues.add(issue);
        }
        return issues;
    }

    @Override
    public void saveIssue(Issue issue) {
        String json = "{" +
                "\"issue\": {" +
                "\"project_id\": " + issue.getProjectId() + "," +
                "\"tracker_id\": " + 3 + "," +
                "\"status_id\": " + 1 + "," +
                "\"subject\": \"" + issue.getSubject() + "\"," +
                "\"description\": \"" + issue.getDescription() + "\"," +
                "\"priority_id\": " + 4 +
                "  }" +
                "}";
        try {
            URL url = new URL(server + "/issues.json");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            setHeaderProperties(con);

            con.getOutputStream().write(json.getBytes("UTF-8"));

            int responseCode = con.getResponseCode();
            StringBuilder response = new StringBuilder();
            if (responseCode == 201) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                ObjectMapper mapper = new ObjectMapper();
                Map map = mapper.readValue(response.toString(), Map.class);
                Map issueObject = (Map) map.get("issue");
                issue.setId((Integer) issueObject.get("id"));
            }
            con.disconnect();
        } catch (Exception e) {
            throw new NotFoundException("can't save issue " + issue, e);
        }
    }

    @Override
    public void saveSpentTime(int issueId, LocalDate date, double hours, String comment) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.ENGLISH);
        DecimalFormat df = new DecimalFormat("##.##", symbols);
        String json = "{" +
                "\"time_entry\": {" +
                "\"issue_id\": " + issueId + "," +
                "\"spent_on\": \"" + date + "\"," +
                "\"hours\": " + df.format(hours) + "," +
                "\"activity_id\": " + 11 + "," +
                "\"comments\": \"" + comment + "\"" +
                "  }" +
                "}";
        try {
            URL url = new URL(server + "/time_entries.json");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            setHeaderProperties(con);

            con.getOutputStream().write(json.getBytes("UTF-8"));
            int responseCode = con.getResponseCode();
            if (responseCode != 201) {
                // TODO Redmine Exception
                System.err.println("save spent time response code: " + responseCode);
            }
            con.disconnect();
        } catch (Exception e) {
            throw new NotFoundException("can't save spent time for issue " + issueId, e);
        }
    }

    private void setHeaderProperties(HttpURLConnection con) {
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "text/html, application/xhtml+xml, */*");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.8,ru;q=0.5,uk;q=0.3");
        con.setRequestProperty("Accept-Encoding", "deflate");
        con.setRequestProperty("X-Redmine-API-Key", key);
    }
}
