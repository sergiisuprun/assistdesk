package com.geekhub.ad.databases;

import com.geekhub.ad.contracts.repositories.CallRepository;
import com.geekhub.ad.entities.Call;
import com.geekhub.ad.entities.Phone;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AsteriskRepository implements CallRepository {

    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<Call> rowMapper;

    public AsteriskRepository(JdbcTemplate jdbcTemplate, RowMapper<Call> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Call> findAllCalls(List<Phone> phones) {
        String sql = sqlSelectCalls("");
        List<Call> calls = jdbcTemplate.query(sql, rowMapper);
        fillPhoneNames(calls, phones);
        return calls;
    }

    @Override
    public List<Call> findProjectCalls(List<Phone> phones) {
        if (phones.size() == 0) {
            return Collections.emptyList();
        }
        String sql = sqlSelectCalls(" WHERE " + sqlCastNumber() + " IN (" + sqlPlaceholders(phones) + ")");
        Object[] numbers = phones.stream()
                .map(Phone::getNumber)
                .toArray();
        List<Call> calls = jdbcTemplate.query(sql, rowMapper, numbers);
        fillPhoneNames(calls, phones);
        return calls;
    }

    private void fillPhoneNames(List<Call> calls, List<Phone> phones) {
        Map<String, String> phonebook = phones.stream()
                .collect(Collectors.toMap(Phone::getNumber, Phone::getName));
        calls.forEach(e -> e.setName(phonebook.get(e.getNumber())));
    }

    private String sqlPlaceholders(List<Phone> phones) {
        String[] arr = new String[phones.size()];
        Arrays.fill(arr, "?");
        return String.join(",", arr);
    }

    private String sqlCastNumber() {
        return "REPLACE(CASE WHEN src='' THEN dst ELSE src END, '+38', '')";
    }

    // TODO: pagination
    private String sqlSelectCalls(String condition) {
        return "SELECT" +
                "  calldate," +
                "  clid," +
                sqlCastNumber() + " AS number," +
                "  duration," +
                "  billsec AS seconds" +
                " FROM cdr" +
                condition +
                " ORDER BY id DESC" +
                " LIMIT 10";
    }
}
