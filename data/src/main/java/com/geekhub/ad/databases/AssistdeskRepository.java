package com.geekhub.ad.databases;

import com.geekhub.ad.contracts.repositories.ProjectRepository;
import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.entities.Project;
import com.geekhub.ad.security.Role;
import com.geekhub.ad.security.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AssistdeskRepository implements UserRepository, ProjectRepository {

    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;
    private final RowMapper<User> userRowMapper;

    public AssistdeskRepository(JdbcTemplate jdbcTemplate, TransactionTemplate transactionTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.transactionTemplate = transactionTemplate;
        this.userRowMapper = new BeanPropertyRowMapper<>(User.class);
    }

    @Override
    public boolean isEmptyUsers() {
        return jdbcTemplate.query("SELECT 1 FROM users", userRowMapper).isEmpty();
    }

    @Override
    public boolean userExists(String username) throws DataAccessException {
        return !jdbcTemplate.query("SELECT 1 FROM users WHERE username = ?", userRowMapper, username).isEmpty();
    }

    @Override
    public List<String> findAllUsernames() throws DataAccessException {
        String sql = "SELECT username FROM users WHERE enabled";
        return jdbcTemplate.queryForList(sql, String.class);
    }

    @Override
    public Optional<User> findUser(String username) throws DataAccessException {
        String sql = "SELECT id, username, password, enabled FROM users WHERE username = ?";

        List<User> users = jdbcTemplate.query(sql, userRowMapper, username);
        if (users.isEmpty()) {
            return Optional.empty();
        }
        User user = users.get(0);

        sql = "SELECT r.id, r.role " +
                "FROM users_roles AS ur" +
                "  JOIN roles AS r ON" +
                "    r.id = ur.role_id AND ur.user_id = ?";

        List<Role> roles = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Role.class), user.getId());
        roles.forEach(user::setRole);
        return Optional.of(user);
    }

    @Override
    public User saveUser(User user) throws DataAccessException {
        transactionTemplate.execute(status -> {
            if (user.isNew()) {
                addUser(user);
                saveRoles(user);
            } else {
                updateUser(user);
                clearRoles(user);
                saveRoles(user);
            }
            return user;
        });
        return user;
    }

    private User updateUser(User user) throws DataAccessException {
        String sql = "UPDATE users SET username = ?, password = ?, enabled = ? WHERE id = ?";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.isEnabled(), user.getId());
        return user;
    }

    private User addUser(User user) throws DataAccessException {
        String sql = "INSERT INTO users (username, password, enabled) VALUES (?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setBoolean(3, user.isEnabled());
            return ps;
        }, keyHolder);
        user.setId(keyHolder.getKey().intValue());
        return user;
    }

    private void clearRoles(User user) {
        String sql = "DELETE FROM users_roles WHERE user_id = ?";
        jdbcTemplate.update(sql, user.getId());
    }

    private void saveRoles(User user) {
        String sql = "INSERT INTO users_roles (user_id, role_id) VALUES (?, ?) ON CONFLICT DO NOTHING";

        List<Object[]> params = new ArrayList<>();
        for (Role role : user.getRoles()) {
            Object[] param = new Object[2];
            param[0] = user.getId();
            param[1] = role.getId();
            params.add(param);
        }
        jdbcTemplate.batchUpdate(sql, params);
    }

    @Override
    public List<Project> findAllProjects() {
        String sql = "SELECT id, name FROM projects WHERE active ORDER BY id";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Project.class));
    }
}
