package com.geekhub.ad.databases;

import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.entities.Phone;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

public class PhonebookRepository implements PhoneRepository {

    private static final String UNKNOWN_NUMBER = "unknown number";

    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<Phone> rowMapper;

    public PhonebookRepository(JdbcTemplate jdbcTemplate, RowMapper<Phone> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Phone> findAllPhones() {
        String sql = "SELECT id, num, cid, projectid FROM cidresolve";
        return jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<Phone> findProjectPhones(int projectId) {
        String sql = "SELECT id, num, cid, projectid FROM cidresolve WHERE projectId = ?";
        return jdbcTemplate.query(sql, rowMapper, projectId);
    }

    @Override
    public Phone getPhoneByNumber(String number) {
        String sql = "SELECT id, num, cid, projectid FROM cidresolve WHERE num = ?";
        List<Phone> phones = jdbcTemplate.query(sql, rowMapper, number);
        return phones.isEmpty() ? getUnknownPhone(number) : phones.get(0);
    }

    private Phone getUnknownPhone(String number) {
        Phone phone = new Phone();
        phone.setNumber(number);
        phone.setName(UNKNOWN_NUMBER);
        return phone;
    }
}
