 Assist Desk 
============================
> Web application for support assistants and managers.
>
> Integrated with telephony and remote access.
>
> To do every day work in one desktop place.

### Use cases ###
* Register
* Login
* Change password
* Navigate projects
* Select current project by incoming call
* Run and stop timer
* Spent time details
* Place new issue
* List issues
* List calls
* List remotes
* Export day report to PDF


### Used components ###
* Spring Boot 2.0
* Spring MVC
* Spring JDBC
* Spring Security
* Thymeleaf
* WebSocket
* Flyway
* Jackson, Gson
* PostgreSQL
* JDK 8

### Environment ###
* [PostgreSQL](https://www.postgresql.org)
* [Asterisk](https://www.asterisk.org)
* [Redmine](https://www.redmine.org)
* [TeamViewer](https://www.teamviewer.com)

### Useful links ###
* [The Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

