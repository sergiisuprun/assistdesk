package com.geekhub.ad.security.profile;

public class ProfileException extends Exception {

    public ProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProfileException(String message) {
        super(message);
    }
}
