package com.geekhub.ad.security.registration;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping
    public String register(Model model) {
        model.addAttribute("registration", new RegistrationRequest());
        return "registrationView";
    }

    @PostMapping
    public String submit(@ModelAttribute("registration") RegistrationRequest registration,
                         BindingResult bindingResult) throws RegistrationException {
        if (bindingResult.hasErrors()) {
            return "registrationView";
        }
        if (registrationService.register(registration)) {
            return "redirect:/";
        }
        return "loginView";
    }
}
