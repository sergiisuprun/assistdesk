package com.geekhub.ad.security.registration;

import com.geekhub.ad.security.Role;
import com.geekhub.ad.security.User;
import com.geekhub.ad.contracts.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public RegistrationService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean register(RegistrationRequest registration) throws RegistrationException {
        String password = registration.getPassword();
        if (!password.equals(registration.getPasswordConfirmation())) {
            throw new RegistrationException("passwords not equals");
        }

        String username = registration.getUsername();
        if (userRepository.userExists(username)) {
            throw new RegistrationException("already registered: " + username);
        }
        try {
            User user = new User();
            user.setUsername(username);
            user.setPassword(passwordEncoder.encode(password));
            user.setRole(Role.roleUser());
            if (userRepository.isEmptyUsers()) {
                user.setRole(Role.roleAdmin());
            }
            user.setEnabled(true);
            userRepository.saveUser(user);
            return true;
        } catch (Exception e) {
            throw new RegistrationException("can't register: " + username, e);
        }
    }
}
