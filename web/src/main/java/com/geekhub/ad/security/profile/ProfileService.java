package com.geekhub.ad.security.profile;

import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.security.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProfileService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public ProfileService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean save(ProfileRequest profile) throws ProfileException {
        String password = profile.getPassword();
        if (!password.equals(profile.getPasswordConfirmation())) {
            throw new ProfileException("passwords not equals");
        }

        Optional<User> optional = userRepository.findUser(profile.getUsername());
        if (optional.isPresent()) {
            User user = optional.get();
            user.setPassword(passwordEncoder.encode(password));
            userRepository.saveUser(user);
            return true;
        }
        return false;
    }
}
