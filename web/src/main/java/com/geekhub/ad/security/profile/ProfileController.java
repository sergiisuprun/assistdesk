package com.geekhub.ad.security.profile;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("profile")
public class ProfileController {

    private final ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @GetMapping
    public String showProfile(Model model) {
        model.addAttribute("profile", new ProfileRequest());
        return "profileView";
    }

    @PostMapping
    public String submit(@ModelAttribute("profile") ProfileRequest profile,
                         BindingResult bindingResult, Principal principal) throws ProfileException {
        profile.setUsername(principal.getName());
        if (bindingResult.hasErrors()) {
            return "profileView";
        }
        if (profileService.save(profile)) {
            return "redirect:/";
        }
        return "profileView";
    }
}
