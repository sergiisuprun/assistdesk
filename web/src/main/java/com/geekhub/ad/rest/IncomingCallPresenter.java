package com.geekhub.ad.rest;

import com.geekhub.ad.contracts.usecases.IncomingCallOutput;
import com.geekhub.ad.entities.Phone;
import com.google.gson.Gson;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import java.util.List;

public class IncomingCallPresenter implements IncomingCallOutput {

    private final SimpMessageSendingOperations messagingTemplate;

    public IncomingCallPresenter(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void onResponse(Phone phone, List<String> usernames) {
        IncomingCallDetails details = new IncomingCallDetails();
        details.setTitle(phone.getName() + " (" + phone.getNumber() + ")");
        details.setUrl("/project/" + phone.getProjectId());

        String data = new Gson().toJson(details);

        for (String username : usernames) {
            messagingTemplate.convertAndSendToUser(username, "/queue/reply", data);
        }
    }
}
