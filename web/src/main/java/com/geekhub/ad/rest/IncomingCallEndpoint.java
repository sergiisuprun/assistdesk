package com.geekhub.ad.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IncomingCallEndpoint {

    private final IncomingCallService incomingCallService;

    public IncomingCallEndpoint(IncomingCallService incomingCallService) {
        this.incomingCallService = incomingCallService;
    }

    @GetMapping("/rest")
    public void getDetails(@RequestParam String number) {
        incomingCallService.showIncomingCall(number);
    }
}
