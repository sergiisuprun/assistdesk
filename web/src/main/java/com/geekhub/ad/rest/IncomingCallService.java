package com.geekhub.ad.rest;

import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.contracts.usecases.IncomingCallInput;
import com.geekhub.ad.usecases.incomingcalls.IncomingCallUseCase;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class IncomingCallService {

    private final IncomingCallInput incomingCall;
    private final SimpMessageSendingOperations messagingTemplate;

    public IncomingCallService(PhoneRepository phoneRepository, UserRepository userRepository,
                               SimpMessageSendingOperations messagingTemplate) {
        this.incomingCall = new IncomingCallUseCase(phoneRepository, userRepository);
        this.messagingTemplate = messagingTemplate;
    }

    void showIncomingCall(String number) {
        if (number.length() <= 10) {
            incomingCall.showIncomingCall(number, new IncomingCallPresenter(messagingTemplate));
        }
    }
}
