package com.geekhub.ad.web.issues;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IssueController {

    private IssueService issueService;

    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @PostMapping("/placeIssue")
    public String placeIssue(@ModelAttribute("issueDetails") IssueDetails issueDetails,
                             BindingResult result) {
        if (result.hasErrors()) {
            return "projectView";
        }

        issueService.placeIssue(issueDetails.getProjectId(),
                issueDetails.getSubject(),
                issueDetails.getDescription());

        return "redirect:/project/" + issueDetails.getProjectId();
    }
}
