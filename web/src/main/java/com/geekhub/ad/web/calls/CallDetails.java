package com.geekhub.ad.web.calls;

import java.time.LocalDateTime;

public class CallDetails {

    private LocalDateTime callDate;
    private String description;
    private int duration;
    private String number;
    private String name;

    public LocalDateTime getCallDate() {
        return callDate;
    }

    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public void setCallDate(LocalDateTime callDate) {
        this.callDate = callDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }
}
