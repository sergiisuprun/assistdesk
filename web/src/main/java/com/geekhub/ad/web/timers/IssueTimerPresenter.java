package com.geekhub.ad.web.timers;

import com.geekhub.ad.contracts.usecases.IssueTimerOutput;
import com.google.gson.Gson;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

public class IssueTimerPresenter implements IssueTimerOutput {

    private final SimpMessageSendingOperations messagingTemplate;

    public IssueTimerPresenter(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void onResponse(int issueId, String username, String time) {
        IssueTimerDetails details = new IssueTimerDetails();
        details.setTitle(time);
        details.setIssueId(issueId);

        String data = new Gson().toJson(details);

        messagingTemplate.convertAndSendToUser(username, "/timer/reply", data);
    }
}
