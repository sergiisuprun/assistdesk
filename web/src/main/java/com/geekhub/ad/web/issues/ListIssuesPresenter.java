package com.geekhub.ad.web.issues;

import com.geekhub.ad.contracts.usecases.ListIssuesOutput;
import com.geekhub.ad.entities.Issue;
import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;

public class ListIssuesPresenter implements ListIssuesOutput {

    private final Model model;

    public ListIssuesPresenter(Model model) {
        this.model = model;
    }

    @Override
    public void onResponse(List<Issue> issues) {

        List<IssueDetails> issueDetails = issues.stream()
                .limit(10)
                .map(e -> {
                    IssueDetails dto = new IssueDetails();
                    dto.setIssueId(e.getId());
                    dto.setProjectId(e.getProjectId());
                    dto.setProjectName(e.getProjectName());
                    dto.setSubject(e.getSubject());
                    dto.setDescription(e.getDescription());
                    return dto;
                })
                .collect(Collectors.toList());

        model.addAttribute("issues", issueDetails);
    }
}
