package com.geekhub.ad.web.timers;

import com.geekhub.ad.contracts.usecases.*;
import com.geekhub.ad.usecases.issues.IssueTimerUseCase;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class IssueTimerService {

    private final SimpMessageSendingOperations messagingTemplate;
    private final IssueTimerInput issueTimer;

    public IssueTimerService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
        this.issueTimer = new IssueTimerUseCase();
    }

    public void startTimer(int issueId, String username) {
        issueTimer.startIssueTimer(issueId, username, new IssueTimerPresenter(messagingTemplate));
    }

    public long stopTimer(int issueId, String username) {
        return issueTimer.stopIssueTimer(issueId, username, new IssueTimerPresenter(messagingTemplate));
    }
}
