package com.geekhub.ad.web.calls;

import com.geekhub.ad.contracts.usecases.ListCallsOutput;
import com.geekhub.ad.entities.Call;
import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;

public class ListCallsPresenter implements ListCallsOutput {

    private final Model model;

    public ListCallsPresenter(Model model) {
        this.model = model;
    }

    @Override
    public void onResponse(List<Call> calls) {
        List<CallDetails> callDetails = calls.stream()
                .map(e -> {
                    CallDetails dto = new CallDetails();
                    dto.setCallDate(e.getCallDate());
                    dto.setDescription(e.getDescription());
                    dto.setDuration(e.getDuration());
                    dto.setNumber(e.getNumber());
                    dto.setName(e.getName());
                    return dto;
                })
                .collect(Collectors.toList());

        model.addAttribute("calls", callDetails);
    }
}
