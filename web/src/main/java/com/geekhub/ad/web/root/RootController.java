package com.geekhub.ad.web.root;

import com.geekhub.ad.web.calls.CallService;
import com.geekhub.ad.entities.Project;
import com.geekhub.ad.web.issues.IssueDetails;
import com.geekhub.ad.web.issues.IssueService;
import com.geekhub.ad.web.projects.ProjectDetails;
import com.geekhub.ad.web.projects.ProjectService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {

    private final CallService callService;
    private final IssueService issueService;
    private final ProjectService projectService;

    public RootController(CallService callService, IssueService issueService, ProjectService projectService) {
        this.callService = callService;
        this.issueService = issueService;
        this.projectService = projectService;
    }

    @GetMapping("/")
    public String assistdesk(Model model) {
        projectService.listProjects(model, Project.ALL);
        issueService.listIssues(model, Project.ALL);
        callService.listCalls(model, Project.ALL);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("username", auth.getName());
        model.addAttribute("projectDetails", new ProjectDetails());
        model.addAttribute("issueDetails", new IssueDetails());

        return "assistdeskView";
    }
}
