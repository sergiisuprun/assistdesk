package com.geekhub.ad.web.calls;

import com.geekhub.ad.contracts.usecases.ListCallsInput;
import com.geekhub.ad.contracts.usecases.ListCallsRequest;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class CallService  {

    private final ListCallsInput listCallsInput;

    public CallService(ListCallsInput listCallsInput) {
        this.listCallsInput = listCallsInput;
    }

    public void listCalls(Model model, int projectId) {
        listCallsInput.listCalls(new ListCallsRequest(projectId), new ListCallsPresenter(model));
    }
}
