package com.geekhub.ad.web.projects;

import com.geekhub.ad.contracts.repositories.ProjectRepository;
import com.geekhub.ad.contracts.usecases.ListProjectsInput;
import com.geekhub.ad.entities.Project;
import com.geekhub.ad.usecases.projects.ListProjectsUseCase;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final ListProjectsInput listProjects;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
        this.listProjects = new ListProjectsUseCase(projectRepository);
    }

    public void listProjects(Model model, int projectId) {
        listProjects.listProjects(new ListProjectsPresenter(model, projectId));
    }

    public List<Project> getAllProjects() {
        return projectRepository.findAllProjects();
    }
}
