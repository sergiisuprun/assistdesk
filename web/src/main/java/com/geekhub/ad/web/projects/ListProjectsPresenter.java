package com.geekhub.ad.web.projects;

import com.geekhub.ad.contracts.usecases.ListProjectsOutput;
import com.geekhub.ad.entities.Project;
import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;

public class ListProjectsPresenter implements ListProjectsOutput {

    private final Model model;
    private final int projectId;

    public ListProjectsPresenter(Model model, int projectId) {
        this.model = model;
        this.projectId = projectId;
    }

    @Override
    public void onResponse(List<Project> projects) {
        List<ProjectDetails> projectDetails = projects.stream()
                .filter(e -> e.getId() > 0)
                .limit(20)
                .map(e -> {
                    ProjectDetails dto = new ProjectDetails();
                    dto.setId(e.getId());
                    dto.setName(e.getName());
                    dto.setCurrent(e.getId() == projectId);
                    return dto;
                })
                .collect(Collectors.toList());

        model.addAttribute("projects", projectDetails);
    }
}
