package com.geekhub.ad.web.time;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/placeTime")
public class SpentTimeController {

    private SpentTimeService spentTimeService;

    public SpentTimeController(SpentTimeService spentTimeService) {
        this.spentTimeService = spentTimeService;
    }

    @GetMapping("/{projectId}/{issueId}")
    public String showSpentTime(@PathVariable("projectId") Integer projectId,
                                @PathVariable("issueId") Integer issueId,
                                @RequestParam("spentSeconds") Integer spentSeconds,
                                @ModelAttribute("details") SpentTimeDetails details) {
        return "spentTimeView";
    }

    @PostMapping
    public String placeSpentTime(@ModelAttribute("details") SpentTimeDetails details,
                                 BindingResult result) {
        if (result.hasErrors()) {
            return "spentTimeView";
        }
        spentTimeService.placeSpentTime(details.getIssueId(), details.getSpentSeconds(), details.getComment());

        return "redirect:/project/" + details.getProjectId();
    }
}