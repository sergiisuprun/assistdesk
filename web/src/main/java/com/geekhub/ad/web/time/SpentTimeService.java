package com.geekhub.ad.web.time;

import com.geekhub.ad.contracts.repositories.SpentTimeRepository;
import com.geekhub.ad.contracts.usecases.*;
import com.geekhub.ad.exceptions.NotFoundException;
import com.geekhub.ad.usecases.time.PlaceSpentTimeUseCase;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class SpentTimeService {

    private final PlaceSpentTimeInput placeSpentTime;

    public SpentTimeService(SpentTimeRepository spentTimeRepository) {
        this.placeSpentTime = new PlaceSpentTimeUseCase(spentTimeRepository);
    }

    public void placeSpentTime(int issueId, int seconds, String comment) throws NotFoundException {
        PlaceSpentTimeRequest request = new PlaceSpentTimeRequest();
        request.setIssueId(issueId);
        request.setDate(LocalDate.now());
        request.setHours(seconds / 3600.0);
        request.setComment(comment);

        PlaceSpentTimeResponse response = new PlaceSpentTimeResponse();

        placeSpentTime.placeSpentTime(request, response);
    }

    private class PlaceSpentTimeResponse implements PlaceSpentTimeOutput {

        @Override
        public void onResponse() {
        }
    }
}
