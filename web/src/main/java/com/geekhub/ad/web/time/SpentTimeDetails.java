package com.geekhub.ad.web.time;

public class SpentTimeDetails {

    private int projectId;
    private int issueId;
    private int spentSeconds;
    private String comment;

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getSpentSeconds() {
        return spentSeconds;
    }

    public void setSpentSeconds(int spentSeconds) {
        this.spentSeconds = spentSeconds;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
