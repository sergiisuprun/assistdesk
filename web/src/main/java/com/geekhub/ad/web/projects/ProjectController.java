package com.geekhub.ad.web.projects;

import com.geekhub.ad.web.calls.CallService;
import com.geekhub.ad.web.issues.IssueDetails;
import com.geekhub.ad.web.issues.IssueService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
public class ProjectController {

    private final CallService callService;
    private final IssueService issueService;
    private final ProjectService projectService;

    public ProjectController(CallService callService, IssueService issueService, ProjectService projectService) {
        this.callService = callService;
        this.issueService = issueService;
        this.projectService = projectService;
    }

    @Secured("ROLE_USER")
    @GetMapping("/project/{projectId}")
    public String project(@PathVariable("projectId") Integer projectId, Model model, Principal principal) {
        projectService.listProjects(model, projectId);
        issueService.listIssues(model, projectId);
        callService.listCalls(model, projectId);

        IssueDetails issueDetails = new IssueDetails();
        issueDetails.setProjectId(projectId);

        model.addAttribute("username", principal.getName());
        model.addAttribute("projectDetails", new ProjectDetails());
        model.addAttribute("issueDetails", issueDetails);

        return "projectView";
    }
}
