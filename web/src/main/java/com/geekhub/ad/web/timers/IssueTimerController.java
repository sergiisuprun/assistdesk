package com.geekhub.ad.web.timers;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Secured("ROLE_USER")
@RequestMapping("/timer")
@Controller
public class IssueTimerController {

    private final IssueTimerService timerService;

    public IssueTimerController(IssueTimerService timerService) {
        this.timerService = timerService;
    }

    @GetMapping("/start/{projectId}/{issueId}")
    public String startTimer(@PathVariable("projectId") Integer projectId,
                             @PathVariable("issueId") Integer issueId,
                             Principal principal) {

        timerService.startTimer(issueId, principal.getName());

        return "redirect:/project/" + projectId;
    }

    @GetMapping("/stop/{projectId}/{issueId}")
    public String stopTimer(@PathVariable("projectId") Integer projectId,
                            @PathVariable("issueId") Integer issueId,
                            Principal principal) {

        long spentSeconds = timerService.stopTimer(issueId, principal.getName());

        return "redirect:/placeTime/" + projectId + "/" + issueId + "?spentSeconds=" + spentSeconds;
    }
}
