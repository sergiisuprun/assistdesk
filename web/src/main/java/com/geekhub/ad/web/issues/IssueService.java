package com.geekhub.ad.web.issues;

import com.geekhub.ad.contracts.repositories.IssueRepository;
import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.contracts.usecases.*;
import com.geekhub.ad.exceptions.NotFoundException;
import com.geekhub.ad.usecases.issues.ListIssuesUseCase;
import com.geekhub.ad.usecases.issues.PlaceIssueUseCase;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class IssueService {

    private final ListIssuesInput listIssues;
    private final PlaceIssueInput placeIssue;

    public IssueService(IssueRepository issueRepository, UserRepository userRepository) {
        this.listIssues = new ListIssuesUseCase(issueRepository);
        this.placeIssue = new PlaceIssueUseCase(issueRepository, userRepository);
    }

    public void listIssues(Model model, int projectId) {
        listIssues.listIssues(new ListIssuesRequest(projectId), new ListIssuesPresenter(model));
    }

    public int placeIssue(int projectId, String subject, String description) throws NotFoundException {

        PlaceIssueRequest request = new PlaceIssueRequest();
        request.setProjectId(projectId);
        request.setSubject(subject);
        request.setDescription(description);

        PlaceIssueResponse response = new PlaceIssueResponse();

        placeIssue.placeIssue(request, response);

        return response.issueId;
    }

    private class PlaceIssueResponse implements PlaceIssueOutput {
        int issueId;

        @Override
        public void onResponse(int issueId) {
            this.issueId = issueId;
        }
    }
}
