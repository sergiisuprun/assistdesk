var ws;

function openSpentTime(projectId, issueId, spentSecounds) {
    window.open('/placeTime/' + projectId + '/' + issueId + '?spentSecounds=' + spentSecounds);
}

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#incomings").removeAttr('hidden');
    $("#incomings").html("Incoming call waiting...");
}

function connect() {
    var ip = location.host;
    var socket = new WebSocket("ws://" + ip + "/incoming");
    ws = Stomp.over(socket);

    ws.connect({}, function () {
        ws.subscribe("/user/queue/reply", function (message) {
            showIncoming(message.body);
        });
        ws.subscribe("/user/timer/reply", function (message) {
            showTimer(message.body);
        });
        setConnected(true);
    }, function () {
        if (ws != null) {
            ws.close();
        }
    });
}

function showIncoming(message) {
    var json = JSON.parse(message);
    $("#incomings")
        .html(json.title)
        .attr("href", json.url);
}

function showTimer(message) {
    var json = JSON.parse(message);
    $("#timer" + json.issueId)
        .html(json.title);
}

// alert("YES, It Works...!!!");
$(function () {
    $("#connect").click(function (e) {
        e.preventDefault();
        connect();
    });
    $("#incomings").click(function () {
        location.assign($(this).attr('href'));
    });
});

function connectWait() {
    window.setTimeout(connect, 1000);
}

window.onload = connectWait;
