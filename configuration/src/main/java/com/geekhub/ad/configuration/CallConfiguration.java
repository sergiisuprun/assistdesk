package com.geekhub.ad.configuration;

import com.geekhub.ad.contracts.repositories.CallRepository;
import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.contracts.usecases.ListCallsInput;
import com.geekhub.ad.databases.AsteriskRepository;
import com.geekhub.ad.entities.Call;
import com.geekhub.ad.usecases.calls.ListCallsUseCase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

@Configuration
@PropertySource("classpath:asterisk.properties")
public class CallConfiguration {

    @Bean
    public DataSource callDataSource(@Value("${call.datasource.url}") String url,
                                     @Value("${call.datasource.username}") String username,
                                     @Value("${call.datasource.password}") String password) {
        return DataSourceBuilder.create()
                .url(url)
                .username(username)
                .password(password)
                .build();
    }

    @Bean
    public JdbcTemplate callJdbcTemplate(@Qualifier("callDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public RowMapper<Call> callRowMapper() {
        return new CallRowMapper();
    }

    @Bean
    public CallRepository callRepository(@Qualifier("callJdbcTemplate") JdbcTemplate jdbcTemplate, RowMapper<Call> callRowMapper) {
        return new AsteriskRepository(jdbcTemplate, callRowMapper);
    }

    @Bean
    public ListCallsInput listCallsInput(CallRepository callRepository, PhoneRepository phoneRepository) {
        return new ListCallsUseCase(callRepository, phoneRepository);
    }

    private class CallRowMapper implements RowMapper<Call> {

        public Call mapRow(ResultSet rs, int rowNum) throws SQLException {
            Call call = new Call();
            call.setCallDate(rs.getTimestamp("calldate").toLocalDateTime());
            call.setDescription(rs.getString("clid"));
            call.setNumber(rs.getString("number"));
            call.setDuration(rs.getInt("duration"));
            call.setSeconds(rs.getInt("seconds"));
            return call;
        }
    }
}
