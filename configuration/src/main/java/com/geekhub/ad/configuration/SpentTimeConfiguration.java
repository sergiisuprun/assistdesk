package com.geekhub.ad.configuration;

import com.geekhub.ad.contracts.repositories.SpentTimeRepository;
import com.geekhub.ad.redmine.RedmineRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:redmine.properties")
public class SpentTimeConfiguration {

    @Bean
    public SpentTimeRepository spentTimeRepository(@Value("${redmine.server}") String server,
                                              @Value("${redmine.key}") String key) {
        return new RedmineRepository(server, key);
    }
}
