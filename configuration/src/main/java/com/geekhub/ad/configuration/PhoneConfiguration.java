package com.geekhub.ad.configuration;

import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.databases.PhonebookRepository;
import com.geekhub.ad.entities.Phone;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

@Configuration
@PropertySource("classpath:phonebook.properties")
public class PhoneConfiguration {

    @Bean
    public DataSource phoneDataSource(@Value("${phone.datasource.url}") String url,
                                      @Value("${phone.datasource.username}") String username,
                                      @Value("${phone.datasource.password}") String password) {
        return DataSourceBuilder.create()
                .url(url)
                .username(username)
                .password(password)
                .build();
    }

    @Bean
    public JdbcTemplate phoneJdbcTemplate(@Qualifier("phoneDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public RowMapper<Phone> phoneRowMapper() {
        return new PhoneRowMapper();
    }

    @Bean
    public PhoneRepository phoneRepository(@Qualifier("phoneJdbcTemplate") JdbcTemplate jdbcTemplate, RowMapper<Phone> phoneRowMapper) {
        return new PhonebookRepository(jdbcTemplate, phoneRowMapper);
    }

    private class PhoneRowMapper implements RowMapper<Phone> {
        public Phone mapRow(ResultSet rs, int rowNum) throws SQLException {
            Phone phone = new Phone();
            phone.setName(rs.getString("cid"));
            phone.setNumber(rs.getString("num"));
            phone.setProjectId(rs.getInt("projectid"));
            return phone;
        }
    }
}
