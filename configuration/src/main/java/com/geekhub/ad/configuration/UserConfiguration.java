package com.geekhub.ad.configuration;

import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.databases.AssistdeskRepository;
import com.geekhub.ad.security.Role;
import com.geekhub.ad.security.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
public class UserConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserRepository userRepository(JdbcTemplate jdbcTemplate, TransactionTemplate transactionTemplate) {
        return new AssistdeskRepository(jdbcTemplate, transactionTemplate);
    }

    @Bean
    public UserDetailsService userDetailsService(UserRepository userRepository) {
        return new UserDetailsServiceImpl(userRepository);
    }

    private class UserDetailsServiceImpl implements UserDetailsService {

        private final UserRepository userRepository;

        private UserDetailsServiceImpl(UserRepository userRepository) {
            this.userRepository = userRepository;
        }

        @Override
        public UserDetails loadUserByUsername(String username) {
            return new UserDetailsImpl(userRepository, username);
        }
    }

    private class UserDetailsImpl implements UserDetails {

        private final User user;

        private UserDetailsImpl(UserRepository userRepository, String username) {
            Optional<User> optional = userRepository.findUser(username);
            if (optional.isPresent()) {
                user = optional.get();
            } else {
                throw new UsernameNotFoundException("user not found: " + username);
            }
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return user.getRoles().stream()
                    .map(GrantedAuthorityImpl::new)
                    .collect(Collectors.toSet());
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return user.isEnabled();
        }
    }

    private class GrantedAuthorityImpl implements GrantedAuthority {
        private final Role role;

        public GrantedAuthorityImpl(Role role) {
            this.role = role;
        }

        @Override
        public String getAuthority() {
            return role.getRole();
        }
    }
}
