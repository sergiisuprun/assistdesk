package com.geekhub.ad.configuration;

import com.geekhub.ad.contracts.repositories.ProjectRepository;
import com.geekhub.ad.databases.AssistdeskRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
public class ProjectConfiguration {

    @Bean
    public ProjectRepository projectRepository(JdbcTemplate jdbcTemplate, TransactionTemplate transactionTemplate) {
        return new AssistdeskRepository(jdbcTemplate, transactionTemplate);
    }
}
