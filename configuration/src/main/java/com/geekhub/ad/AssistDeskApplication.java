package com.geekhub.ad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssistDeskApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssistDeskApplication.class, args);
    }
}
