package com.geekhub.ad.usecases.issues;

import com.geekhub.ad.contracts.repositories.IssueRepository;
import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.contracts.usecases.PlaceIssueInput;
import com.geekhub.ad.contracts.usecases.PlaceIssueOutput;
import com.geekhub.ad.contracts.usecases.PlaceIssueRequest;
import com.geekhub.ad.entities.Issue;

public class PlaceIssueUseCase implements PlaceIssueInput {

    private final IssueRepository issueRepository;
    private final UserRepository userRepository;

    public PlaceIssueUseCase(IssueRepository issueRepository, UserRepository userRepository) {
        this.issueRepository = issueRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void placeIssue(PlaceIssueRequest request, PlaceIssueOutput output) {
        int projectId = request.getProjectId();
        String subject = request.getSubject();
        String description = request.getDescription();

        ValidateIssue.newInstance(userRepository).validate(projectId, subject, description);
        int issueId = storeIssue(projectId, subject, description);

        output.onResponse(issueId);
    }

    private int storeIssue(int projectId, String subject, String description) {
        Issue issue = new Issue();
        issue.setProjectId(projectId);
        issue.setSubject(subject);
        issue.setDescription(description);
        issueRepository.saveIssue(issue);
        return issue.getId();
    }
}
