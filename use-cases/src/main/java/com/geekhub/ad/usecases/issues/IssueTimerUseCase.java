package com.geekhub.ad.usecases.issues;

import com.geekhub.ad.contracts.usecases.IssueTimerInput;
import com.geekhub.ad.contracts.usecases.IssueTimerOutput;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class IssueTimerUseCase implements IssueTimerInput {

    private final Map<UserIssue, IssueTimer> timers;

    public IssueTimerUseCase() {
        this.timers = new ConcurrentHashMap<>();
    }

    @Override
    public void startIssueTimer(int issueId, String username, IssueTimerOutput output) {
        UserIssue userIssue = new UserIssue(issueId, username);
        if (!timers.containsKey(userIssue)) {
            IssueTimer timer = new IssueTimer(userIssue, output);
            timers.put(userIssue, timer);
            timer.start();
        }
    }

    @Override
    public long stopIssueTimer(int issueId, String username, IssueTimerOutput output) {
        UserIssue userIssue = new UserIssue(issueId, username);
        IssueTimer timer = timers.get(userIssue);
        if (timer != null) {
            timer.stop();
            timers.remove(userIssue);
            return timer.getInterval();
        }
        return 0;
    }

    private class UserIssue {

        private final int issueId;
        private final String username;

        private UserIssue(int issueId, String username) {
            this.issueId = issueId;
            this.username = username;
        }

        public int getIssueId() {
            return issueId;
        }

        public String getUsername() {
            return username;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            UserIssue userIssue = (UserIssue) o;
            return issueId == userIssue.issueId &&
                    Objects.equals(username, userIssue.username);
        }

        @Override
        public int hashCode() {
            return Objects.hash(issueId, username);
        }
    }

    private class IssueTimerTask extends TimerTask {

        private final IssueTimer issueTimer;
        private final IssueTimerOutput output;

        private IssueTimerTask(IssueTimer issueTimer, IssueTimerOutput output) {
            this.issueTimer = issueTimer;
            this.output = output;
        }

        @Override
        public void run() {
            Long interval = issueTimer.getInterval();
            UserIssue userIssue = issueTimer.getUserIssue();
            output.onResponse(userIssue.getIssueId(), userIssue.getUsername(), interval.toString());
        }
    }

    private class IssueTimer {

        private final UserIssue userIssue;
        private final IssueTimerOutput output;
        private final Timer timer;
        private LocalDateTime startTime;
        private LocalDateTime stopTime;
        private boolean started;
        private boolean stopped;

        private IssueTimer(UserIssue userIssue, IssueTimerOutput output) {
            this.userIssue = userIssue;
            this.output = output;
            this.timer = new Timer();
        }

        private void start() {
            timer.scheduleAtFixedRate(new IssueTimerTask(this, output), 0, 1000);
            startTime = LocalDateTime.now();
            started = true;
        }

        private void stop() {
            timer.cancel();
            stopTime = LocalDateTime.now();
            stopped = true;
        }

        private long getInterval() {
            LocalDateTime time = stopped ? stopTime : LocalDateTime.now();
            long seconds = started ? startTime.until(time, ChronoUnit.SECONDS) : 0;
            return seconds;
        }

        private UserIssue getUserIssue() {
            return userIssue;
        }
    }
}
