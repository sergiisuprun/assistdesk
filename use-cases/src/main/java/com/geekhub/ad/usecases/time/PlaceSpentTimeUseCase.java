package com.geekhub.ad.usecases.time;

import com.geekhub.ad.contracts.repositories.SpentTimeRepository;
import com.geekhub.ad.contracts.usecases.PlaceSpentTimeInput;
import com.geekhub.ad.contracts.usecases.PlaceSpentTimeOutput;
import com.geekhub.ad.contracts.usecases.PlaceSpentTimeRequest;

public class PlaceSpentTimeUseCase implements PlaceSpentTimeInput {

    private final SpentTimeRepository spentTimeRepository;

    public PlaceSpentTimeUseCase(SpentTimeRepository spentTimeRepository) {
        this.spentTimeRepository = spentTimeRepository;
    }

    @Override
    public void placeSpentTime(PlaceSpentTimeRequest request, PlaceSpentTimeOutput output) {
        double hours = Double.max(request.getHours(), SpentTimeRepository.MIN_SPENT_TIME_ALLOWED);
        spentTimeRepository.saveSpentTime(request.getIssueId(), request.getDate(), hours, request.getComment());
        output.onResponse();
    }
}
