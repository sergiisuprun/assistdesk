package com.geekhub.ad.usecases.issues;

import com.geekhub.ad.contracts.repositories.IssueRepository;
import com.geekhub.ad.contracts.usecases.ListIssuesInput;
import com.geekhub.ad.contracts.usecases.ListIssuesOutput;
import com.geekhub.ad.contracts.usecases.ListIssuesRequest;
import com.geekhub.ad.entities.Issue;
import com.geekhub.ad.entities.Project;

import java.util.List;

public class ListIssuesUseCase implements ListIssuesInput {

    private final IssueRepository issueRepository;

    public ListIssuesUseCase(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public List<Issue> listIssues() {
        return issueRepository.findAllIssues();
    }

    @Override
    public void listIssues(ListIssuesRequest request, ListIssuesOutput output) {
        int projectId = request.getProjectId();

        ValidateIssue.newInstance(null).validate(projectId);

        List<Issue> issues = (projectId == Project.ALL)
                ? issueRepository.findAllIssues()
                : issueRepository.findProjectIssues(projectId);

        output.onResponse(issues);
    }
}
