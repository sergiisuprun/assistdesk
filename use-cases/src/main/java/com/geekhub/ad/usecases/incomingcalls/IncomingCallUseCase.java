package com.geekhub.ad.usecases.incomingcalls;

import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.contracts.usecases.IncomingCallInput;
import com.geekhub.ad.contracts.usecases.IncomingCallOutput;
import com.geekhub.ad.entities.Phone;

import java.util.List;

public class IncomingCallUseCase implements IncomingCallInput {

    private final PhoneRepository phoneRepository;
    private final UserRepository userRepository;

    public IncomingCallUseCase(PhoneRepository phoneRepository, UserRepository userRepository) {
        this.phoneRepository = phoneRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void showIncomingCall(String number, IncomingCallOutput incomingCallOutput) {
        Phone phone = phoneRepository.getPhoneByNumber(number);
        List<String> usernames = userRepository.findAllUsernames();
        incomingCallOutput.onResponse(phone, usernames);
    }
}
