package com.geekhub.ad.usecases.projects;

import com.geekhub.ad.contracts.repositories.ProjectRepository;
import com.geekhub.ad.contracts.usecases.ListProjectsInput;
import com.geekhub.ad.contracts.usecases.ListProjectsOutput;
import com.geekhub.ad.entities.Project;

import java.util.List;

public class ListProjectsUseCase implements ListProjectsInput {

    private final ProjectRepository projectRepository;

    public ListProjectsUseCase(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    public List<Project> listProjects() {
        return projectRepository.findAllProjects();
    }

    @Override
    public void listProjects(ListProjectsOutput output) {
        output.onResponse(listProjects());
    }
}
