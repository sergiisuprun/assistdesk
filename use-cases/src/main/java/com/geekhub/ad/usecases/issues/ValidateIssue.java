package com.geekhub.ad.usecases.issues;

import com.geekhub.ad.contracts.repositories.UserRepository;
import com.geekhub.ad.exceptions.NotFoundException;

public class ValidateIssue {

    private final UserRepository userRepository;

    private ValidateIssue(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static ValidateIssue newInstance(UserRepository userRepository) {
        return new ValidateIssue(userRepository);
    }

    public void validate(int projectId) {
        validateProject(projectId);
    }

    public void validate(int projectId, String subject, String description) {
        validateProject(projectId);
        validateSubject(subject);
        validateDescription(description);
    }

    private void validateProject(int projectId) {
        if (projectId == 0) {
            // TODO: throw new NotFoundException("project id " + projectId + " not found");
        }
    }

    private void validateSubject(String subject) {
        if (subject.length() < 5) {
            throw new NotFoundException("subject not valid");
        }
    }

    private void validateDescription(String description) {
        if (description.length() < 5) {
            throw new NotFoundException("description not valid");
        }
    }
}
