package com.geekhub.ad.usecases.calls;

import com.geekhub.ad.contracts.repositories.CallRepository;
import com.geekhub.ad.contracts.repositories.PhoneRepository;
import com.geekhub.ad.contracts.usecases.ListCallsInput;
import com.geekhub.ad.contracts.usecases.ListCallsOutput;
import com.geekhub.ad.contracts.usecases.ListCallsRequest;
import com.geekhub.ad.entities.Phone;
import com.geekhub.ad.entities.Project;

import java.util.List;

public class ListCallsUseCase implements ListCallsInput {

    private final CallRepository callRepository;
    private final PhoneRepository phoneRepository;

    public ListCallsUseCase(CallRepository callRepository, PhoneRepository phoneRepository) {
        this.callRepository = callRepository;
        this.phoneRepository = phoneRepository;
    }

    @Override
    public void listCalls(ListCallsRequest request, ListCallsOutput listItemsOutput) {
        int projectId = request.getProjectId();
        if (projectId == Project.ALL) {
            List<Phone> phones = phoneRepository.findAllPhones();
            listItemsOutput.onResponse(callRepository.findAllCalls(phones));
        } else {
            List<Phone> phones = phoneRepository.findProjectPhones(projectId);
            listItemsOutput.onResponse(callRepository.findProjectCalls(phones));
        }
    }
}
