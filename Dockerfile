FROM gradle:jdk8 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --info

FROM openjdk:8-jre-alpine
EXPOSE 80
COPY --from=builder /home/gradle/src/build/distributions/assistdesk.tar /app/
WORKDIR /app
RUN tar -xvf assistdesk.tar
RUN rm /app/assistdesk.tar
WORKDIR /app/assistdesk
CMD bin/assistdesk
