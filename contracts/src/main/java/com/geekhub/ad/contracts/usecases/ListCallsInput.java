package com.geekhub.ad.contracts.usecases;

public interface ListCallsInput {

    void listCalls(ListCallsRequest request, ListCallsOutput presenter);

}
