package com.geekhub.ad.contracts.usecases;

public interface PlaceIssueOutput {

    void onResponse(int issueId);

}
