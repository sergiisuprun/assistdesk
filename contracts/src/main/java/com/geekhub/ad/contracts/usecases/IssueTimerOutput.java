package com.geekhub.ad.contracts.usecases;

public interface IssueTimerOutput {

    void onResponse(int issueId, String username, String time);

}
