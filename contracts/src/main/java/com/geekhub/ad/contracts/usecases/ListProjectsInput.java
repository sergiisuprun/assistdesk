package com.geekhub.ad.contracts.usecases;

public interface ListProjectsInput {

    void listProjects(ListProjectsOutput output);

}
