package com.geekhub.ad.contracts.repositories;

import com.geekhub.ad.security.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Optional<User> findUser(String username);

    User saveUser(User user);

    List<String> findAllUsernames();

    boolean userExists(String username);

    boolean isEmptyUsers();
}
