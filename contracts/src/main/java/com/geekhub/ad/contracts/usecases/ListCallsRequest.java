package com.geekhub.ad.contracts.usecases;

public class ListCallsRequest {

    private int projectId;

    public ListCallsRequest(int projectId) {
        this.projectId = projectId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
