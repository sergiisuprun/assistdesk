package com.geekhub.ad.contracts.repositories;

import java.time.LocalDate;

public interface SpentTimeRepository {

    double MIN_SPENT_TIME_ALLOWED = 0.01;

    void saveSpentTime(int issueId, LocalDate date, double hours, String comment);

}
