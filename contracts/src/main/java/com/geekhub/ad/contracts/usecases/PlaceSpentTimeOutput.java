package com.geekhub.ad.contracts.usecases;

public interface PlaceSpentTimeOutput {

    void onResponse();

}
