package com.geekhub.ad.contracts.usecases;

public interface ListIssuesInput {

    void listIssues(ListIssuesRequest request, ListIssuesOutput output);

}
