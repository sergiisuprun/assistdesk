package com.geekhub.ad.contracts.usecases;

public interface PlaceIssueInput {

    void placeIssue(PlaceIssueRequest request, PlaceIssueOutput output);

}
