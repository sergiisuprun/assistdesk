package com.geekhub.ad.contracts.usecases;

import com.geekhub.ad.entities.Phone;

import java.util.List;

public interface IncomingCallOutput {

    void onResponse(Phone phone, List<String> usernames);

}
