package com.geekhub.ad.contracts.usecases;

public interface PlaceSpentTimeInput {

    void placeSpentTime(PlaceSpentTimeRequest request, PlaceSpentTimeOutput output);

}
