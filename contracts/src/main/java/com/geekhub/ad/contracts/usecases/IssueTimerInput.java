package com.geekhub.ad.contracts.usecases;

public interface IssueTimerInput {

    void startIssueTimer(int issueId, String username, IssueTimerOutput output);

    long stopIssueTimer(int issueId, String username, IssueTimerOutput output);

}
