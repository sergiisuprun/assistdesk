package com.geekhub.ad.contracts.repositories;

import com.geekhub.ad.entities.Call;
import com.geekhub.ad.entities.Phone;

import java.util.List;

public interface CallRepository {

    List<Call> findAllCalls(List<Phone> phones);

    List<Call> findProjectCalls(List<Phone> phones);

}
