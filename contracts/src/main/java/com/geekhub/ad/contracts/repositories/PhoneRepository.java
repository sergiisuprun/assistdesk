package com.geekhub.ad.contracts.repositories;

import com.geekhub.ad.entities.Phone;

import java.util.List;

public interface PhoneRepository {

    List<Phone> findAllPhones();

    List<Phone> findProjectPhones(int projectId);

    Phone getPhoneByNumber(String number);

}
