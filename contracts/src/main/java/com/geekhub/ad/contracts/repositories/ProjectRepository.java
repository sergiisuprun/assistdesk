package com.geekhub.ad.contracts.repositories;

import com.geekhub.ad.entities.Project;

import java.util.List;

public interface ProjectRepository {

    List<Project> findAllProjects();

}
