package com.geekhub.ad.contracts.usecases;

import com.geekhub.ad.entities.Issue;

import java.util.List;

public interface ListIssuesOutput {

    void onResponse(List<Issue> issues);

}
