package com.geekhub.ad.contracts.usecases;

import com.geekhub.ad.entities.Project;

import java.util.List;

public interface ListProjectsOutput {

    void onResponse(List<Project> issues);

}
