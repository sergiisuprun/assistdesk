package com.geekhub.ad.contracts.usecases;

public interface IncomingCallInput {

    void showIncomingCall(String number, IncomingCallOutput incomingCallOutput);

}
