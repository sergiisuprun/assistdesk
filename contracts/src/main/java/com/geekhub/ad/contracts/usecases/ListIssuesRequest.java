package com.geekhub.ad.contracts.usecases;

public class ListIssuesRequest {

    private int projectId;

    public ListIssuesRequest(int projectId) {
        this.projectId = projectId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
