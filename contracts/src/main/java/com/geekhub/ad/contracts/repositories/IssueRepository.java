package com.geekhub.ad.contracts.repositories;

import com.geekhub.ad.entities.Issue;

import java.util.List;

public interface IssueRepository {

    List<Issue> findAllIssues();

    List<Issue> findProjectIssues(int projectId);

    void saveIssue(Issue issue);

}
