package com.geekhub.ad.contracts.usecases;

import com.geekhub.ad.entities.Call;
import com.geekhub.ad.entities.Phone;

import java.util.List;

public interface ListCallsOutput {

    void onResponse(List<Call> calls);

}
