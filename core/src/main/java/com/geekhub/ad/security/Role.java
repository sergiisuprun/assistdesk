package com.geekhub.ad.security;

import java.io.Serializable;

public class Role implements Serializable {

    private int id;
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static Role roleAdmin() {
        Role role = new Role();
        role.id = 1;
        role.role = "ROLE_ADMIN";
        return role;
    }

    public static Role roleUser() {
        Role role = new Role();
        role.id = 10;
        role.role = "ROLE_USER";
        return role;
    }
}
