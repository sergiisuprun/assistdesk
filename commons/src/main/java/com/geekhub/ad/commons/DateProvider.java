package com.geekhub.ad.commons;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateProvider {

    private final LocalDateTime dateTime;

    public DateProvider(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public DateProvider() {
        this.dateTime = LocalDateTime.now();
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public LocalDate getDate() {
        return dateTime.toLocalDate();
    }

    public boolean isWorkingTime() {
        int hour = dateTime.getHour();
        return hour > 9 && hour < 17;
    }
}
